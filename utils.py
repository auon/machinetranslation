import sys
from datetime import datetime

import numpy as np


def softmax(x):
    xt = np.exp(x - np.max(x))
    return xt / np.sum(xt)


def tanh(x):
    if x > 0:
        return (1 - np.exp(-2 * x)) / (1 + np.exp(-2 * x))
    else:
        return (np.exp(2 * x) - 1) / (np.exp(2 * x) + 1)


tanh = np.vectorize(tanh, otypes=[np.float])


def get_words_from_indices(index_to_word, x):
    return [index_to_word[w] for w in x]


def get_indices_from_words(word_to_index, x):
    unknown_word = "UNKNOWN_WORD"
    unknown_word_index = 0
    if unknown_word in word_to_index:
        unknown_word_index = word_to_index[unknown_word]

    return [word_to_index.get(w, unknown_word_index) for w in x]


def get_indices_from_words_with_unk(word_to_index, x):
    unknown_word = "UNKNOWN_WORD"
    return [word_to_index.get(w, word_to_index[unknown_word]) for w in x]


def get_dict_from_model(model, x_index_to_word, x_word_to_index, y_index_to_word, y_word_to_index, learning_rate):
    return {'source_lang_dim': model.source_lang_dim,
            'target_lang_dim': model.target_lang_dim,
            'hidden_layer_dim': model.hidden_layer_dim,
            'sentence_end_index_target': model.sentence_end_index_target,
            'W': model.W,
            'U': model.U,
            'B': model.B,
            'G': model.G,
            'W_d': model.W_d,
            'U_d': model.U_d,
            'B_d': model.B_d,
            'x_index_to_word': x_index_to_word,
            'x_word_to_index': x_word_to_index,
            'y_index_to_word': y_index_to_word,
            'y_word_to_index': y_word_to_index,
            'learning_rate': learning_rate}


def get_model_from_dict(model, model_dict):
    model.source_lang_dim = model_dict['source_lang_dim']
    model.target_lang_dim = model_dict['target_lang_dim']
    model.hidden_layer_dim = model_dict['hidden_layer_dim']
    model.sentence_end_index_target = model_dict['sentence_end_index_target']
    model.W = model_dict['W']
    model.U = model_dict['U']
    model.B = model_dict['B']
    model.G = model_dict['G']
    model.W_d = model_dict['W_d']
    model.U_d = model_dict['U_d']
    model.B_d = model_dict['B_d']


def get_dict_from_training_data(model):
    return {'source_lang_dim': model.source_lang_dim,
            'target_lang_dim': model.target_lang_dim}


def get_training_data_from_dict(model, model_dict):
    model.source_lang_dim = model_dict['source_lang_dim']
    model.target_lang_dim = model_dict['target_lang_dim']


def train_with_sgd(model, x_train, y_train, learning_rate=0.005, num_of_epochs=100, evaluate_error_interval=5):
    # Keeping track of the errors for plotting later
    errors = []
    examples_seen = 0
    for epoch in range(num_of_epochs):
        # Optionally evaluating the error
        if epoch % evaluate_error_interval == 0:
            error = model.calculate_error(x_train, y_train)
            errors.append((examples_seen, error))
            time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print("%s: Loss after examples_seen=%d epoch=%d: %f" % (time, examples_seen, epoch, error))
            # Adjusting the learning rate for increasing error
            if len(errors) > 1 and errors[-1][1] > errors[-2][1]:
                learning_rate *= 0.5
                print("Setting learning rate to %f" % learning_rate)
                sys.stdout.flush()

        # For each training example
        for i in range(len(y_train)):
            # One SGD step
            model.sgd_step(x_train[i], y_train[i], learning_rate)
            examples_seen += 1

    return learning_rate
