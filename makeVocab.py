f1 = open("dev.en", "r")
vocab = set()
for row in f1:
	x = row.strip().lower().split(' ')
	temp = set(list(vocab) + list(x))
	vocab = temp
print ("No. of words in English vocabulary: ",len(vocab))
f2 = open("vocab.en","w")
f2.write(str(vocab))
f1.close()
f2.close()

f1 = open("dev.hi", "r")
vocab = set()
for row in f1:
	x = row.strip().split(' ')
	temp = set(list(vocab) + list(x))
	vocab = temp
print ("No. of words in Hindi vocabulary: ",len(vocab))
f2 = open("vocab.hi","w")
f2.write(str(vocab))
f1.close()
f2.close()
