import numpy as np

from utils import softmax, tanh


class RNNTranslator:
    def __init__(self, source_lang_dim, target_lang_dim, sentence_end_index_target, hidden_layer_dim=100):
        # Assigning variables
        self.source_lang_dim = source_lang_dim
        self.target_lang_dim = target_lang_dim
        self.hidden_layer_dim = hidden_layer_dim
        self.sentence_end_index_target = sentence_end_index_target

        # Randomly initializing the weights
        self.W = np.random.uniform(
            -np.sqrt(1. / source_lang_dim), np.sqrt(1. / source_lang_dim), (hidden_layer_dim, source_lang_dim))
        self.U = np.random.uniform(
            -np.sqrt(1. / hidden_layer_dim), np.sqrt(1. / hidden_layer_dim), (hidden_layer_dim, hidden_layer_dim))
        self.B = np.random.uniform(-1., 1., hidden_layer_dim)

        self.G = np.random.uniform(
            -np.sqrt(1. / hidden_layer_dim), np.sqrt(1. / hidden_layer_dim), (target_lang_dim, hidden_layer_dim))
        self.W_d = np.random.uniform(
            -np.sqrt(1. / target_lang_dim), np.sqrt(1. / target_lang_dim), (hidden_layer_dim, target_lang_dim))
        self.U_d = np.random.uniform(
            -np.sqrt(1. / hidden_layer_dim), np.sqrt(1. / hidden_layer_dim), (hidden_layer_dim, hidden_layer_dim))
        self.B_d = np.random.uniform(-1., 1., hidden_layer_dim)

    def feed_forward(self, x, y=None, train=False):
        # Total number of time steps in encoder
        time_steps_encoder = len(x)

        # Encoder

        # Saving all hidden states in h
        # Adding an additional element for the initial hidden state
        h = np.zeros((time_steps_encoder + 1, self.hidden_layer_dim))
        # h[-1] = np.zeros(self.hidden_layer_dim)

        # For each time step
        for t in np.arange(time_steps_encoder):
            # Indexing W by x[t]. Same as multiplying W with the one-hot vector.
            h[t] = tanh(self.W[:, x[t]] + self.U.dot(h[t - 1]) + self.B)

        # Encoding
        c = h[time_steps_encoder - 1]

        # Decoder
        # Restricting the number of time steps in decoder to 2 * time_steps_encoder in case of testing
        if train is False:
            time_steps_decoder = 2 * time_steps_encoder
        else:
            time_steps_decoder = len(y)

        h_d = np.zeros((time_steps_decoder, self.hidden_layer_dim))

        # Saving all predictions in y_h
        y_h = np.zeros((time_steps_decoder, self.target_lang_dim))

        # For each time step
        h_d[0] = tanh(self.B_d + c)
        y_h[0] = softmax(self.G.dot(h_d[0]))
        for t in np.arange(1, time_steps_decoder):
            # Indexing W_d by y_h[t - 1]. Same as multiplying W with the one-hot vector.
            if not train:
                h_d[t] = tanh(self.W_d.dot(y_h[t - 1]) + self.U_d.dot(h_d[t - 1]) + self.B_d + c)
            else:
                h_d[t] = tanh(self.W_d[:, y[t - 1]] + self.U_d.dot(h_d[t - 1]) + self.B_d + c)

            y_h[t] = softmax(self.G.dot(h_d[t]))

            # Stop when sentence end is found
            if not train and np.argmax(y_h[t]) == self.sentence_end_index_target:
                # Length of the output is lesser than the restricted value
                if t != time_steps_decoder - 1:
                    y_h = y_h[:t + 1]
                    h_d = h_d[:t + 1]

                break

        return [y_h, h, h_d]

    def predict(self, x):
        # Perform forward propagation and return index of the highest score
        y_h, h, h_d = self.feed_forward(x)
        return np.argmax(y_h, axis=1)

    def calculate_total_error(self, x, y):
        l = 0
        # For each sentence pair
        for i in np.arange(len(y)):
            # print("Seeing example %s" % i)
            y_h, h, h_d = self.feed_forward(x[i], y[i], True)
            # Calculating probabilities of the correct words
            correct_word_predictions = y_h[np.arange(len(y[i])), y[i]]
            # Adding log of these probabilities to the loss
            l += -1 * np.sum(np.log(correct_word_predictions))
        return l

    def calculate_error(self, x, y):
        # Dividing the total loss by number of training examples
        n = len(y)
        return self.calculate_total_error(x, y) / n

    def bptt(self, x, y):
        time_steps_decoder = len(y)
        time_steps_encoder = len(x)

        # Forward propagation
        y_h, h, h_d = self.feed_forward(x, y, True)

        # Initializing the gradients
        dEdW = np.zeros(self.W.shape)
        dEdU = np.zeros(self.U.shape)
        dEdB = np.zeros(self.B.shape)
        dEdG = np.zeros(self.G.shape)
        dEdW_d = np.zeros(self.W_d.shape)
        dEdU_d = np.zeros(self.U_d.shape)
        dEdB_d = np.zeros(self.B_d.shape)

        dEdh_d = np.zeros(h_d.shape)
        dEdc = np.zeros(h[0].shape)
        dEdh = np.zeros(h.shape)

        dEdO = y_h
        dEdO[np.arange(time_steps_decoder), y] -= 1.

        dEdO_mul_G = dEdO.dot(self.G)

        # For each time step backwards of Decoder
        for t in np.arange(time_steps_decoder)[::-1]:
            # Calculating dE/dh_d
            if t == time_steps_decoder - 1:
                dEdh_d[t] = dEdO_mul_G[t]
            else:
                dEdh_d[t] = dEdh_d[t + 1].dot(np.diag(1 - h_d[t + 1] ** 2).dot(self.U_d)) + dEdO_mul_G[t]

            # There is no change in some weights in case of the first time step
            if t != 0:
                # Calculating dE/dW_d
                dEdW_d[:, y[t - 1]] += dEdh_d[t] * (1 - h_d[t] ** 2)

                # Calculating dE/dU_d
                dEdU_d += np.outer(dEdh_d[t] * (1 - h_d[t] ** 2), h_d[t - 1])

            # Calculating dE/dB_d
            dEdB_d += dEdh_d[t] * (1 - h_d[t] ** 2)

            # Calculating dE/dc
            dEdc += dEdh_d[t] * (1 - h_d[t] ** 2)

        # For each time step backwards of Encoder
        for t in np.arange(time_steps_encoder)[::-1]:
            # Calculating dE/dh
            if t == time_steps_encoder - 1:
                dEdh[t] = dEdc
            else:
                dEdh[t] = dEdh[t + 1].dot(np.diag(1 - h[t + 1] ** 2).dot(self.U))

            # Calculating dE/dW
            dEdW[:, x[t]] += dEdh[t] * (1 - h[t] ** 2)

            # There is no change in some weights in case of the first time step
            if t != 0:
                # Calculating dE/dU
                dEdU += np.outer(dEdh[t] * (1 - h[t] ** 2), h[t - 1])

            # Calculating dE/dB
            dEdB_d += dEdh[t] * (1 - h[t] ** 2)

        return [dEdW, dEdU, dEdB, dEdG, dEdW_d, dEdU_d, dEdB_d]

    def sgd_step(self, x, y, learning_rate):
        # Calculating gradients
        dEdW, dEdU, dEdB, dEdG, dEdW_d, dEdU_d, dEdB_d = self.bptt(x, y)

        # Updating weights
        self.W -= learning_rate * dEdW
        self.U -= learning_rate * dEdU
        self.B -= learning_rate * dEdB
        self.G -= learning_rate * dEdG
        self.W_d -= learning_rate * dEdW_d
        self.U_d -= learning_rate * dEdU_d
        self.B_d -= learning_rate * dEdB_d

    def __getstate__(self):
        return self.source_lang_dim, self.target_lang_dim, self.hidden_layer_dim, self.sentence_end_index_target, self.W, self.U, self.B, self.G, self.W_d, self.U_d, self.B_d

    def __setstate__(self, state):
        source_lang_dim, target_lang_dim, hidden_layer_dim, sentence_end_index_target, W, U, B, G, W_d, U_d, B_d = state
        self.source_lang_dim = source_lang_dim
        self.target_lang_dim = target_lang_dim
        self.hidden_layer_dim = hidden_layer_dim
        self.sentence_end_index_target = sentence_end_index_target
        self.W = W
        self.U = U
        self.B = B
        self.G = G
        self.W_d = W_d
        self.U_d = U_d
        self.B_d = B_d
