import time

import numpy as np
from builtins import print
from numpy import *
import pickle

from RNNTranslator import RNNTranslator
from utils import train_with_sgd, get_dict_from_model, get_model_from_dict
from word_to_vec import generate_features, get_features, generate_features_with_unknown_words
import sys

# Parameters
hidden_layer_dim = 50
learning_rate = 0.01
num_of_epochs = 10000
evaluate_error_interval = 50

source_lang_filename = "smaller_50.en"
target_lang_filename = "smaller_50.hi"
# source_read_first_n_lines = 10
# target_read_first_n_lines = 10

# Restricting vocab size
vocab_size = 250

if len(sys.argv) > 1:
    source_lang_filename = sys.argv[1] + ".en"
    target_lang_filename = sys.argv[1] + ".hi"

    if len(sys.argv) > 2:
        num_of_epochs = int(sys.argv[2])

        if len(sys.argv) > 3:
            hidden_layer_dim = int(sys.argv[3])

model_file_name = "model_" + source_lang_filename[:-3] + "_hidden_" + str(hidden_layer_dim) + ".save"

try:
    with open(model_file_name, "r") as f:
        # Model file present, loading the model
        print("Loading model from file: %s" % model_file_name)
        model_dict = eval(f.read().strip())

        x_index_to_word = model_dict['x_index_to_word']
        x_word_to_index = model_dict['x_word_to_index']
        x_dimen = len(model_dict['x_index_to_word'])
        x_train = get_features(source_lang_filename, x_word_to_index)
        y_index_to_word = model_dict['y_index_to_word']
        y_word_to_index = model_dict['y_word_to_index']
        y_dimen = len(model_dict['y_index_to_word'])
        y_train = get_features(target_lang_filename, y_word_to_index)
        learning_rate = model_dict['learning_rate']

        model = RNNTranslator(x_dimen, y_dimen, y_word_to_index['SENTENCE_END'])

        get_model_from_dict(model, model_dict)
except OSError:
    # Model file not present
    print("Model file not present")
    # Load file and create vectors
    # x_train, x_index_to_word, x_word_to_index, x_dimen = generate_features(source_lang_filename)
    # y_train, y_index_to_word, y_word_to_index, y_dimen = generate_features(target_lang_filename)
    x_train, x_index_to_word, x_word_to_index, x_dimen = generate_features_with_unknown_words(
        source_lang_filename, vocab_size)
    y_train, y_index_to_word, y_word_to_index, y_dimen = generate_features_with_unknown_words(
        target_lang_filename, vocab_size)
    model = RNNTranslator(x_dimen, y_dimen, y_word_to_index['SENTENCE_END'], hidden_layer_dim=hidden_layer_dim)

print("Number of hidden layers: %d" % hidden_layer_dim)
print("Number of epochs: %d" % num_of_epochs)

# np.random.seed(10)
start_time = time.time()
# model.sgd_step(x_train[10], y_train[10], 0.005)
learning_rate = train_with_sgd(
    model, x_train, y_train,
    learning_rate=learning_rate, num_of_epochs=num_of_epochs, evaluate_error_interval=evaluate_error_interval)
elapsed_time = time.time() - start_time
print("Time elapsed = %f" % elapsed_time)
print("Expected Loss for random predictions: %f" % np.log(y_dimen))
print("Actual loss: %f" % model.calculate_error(x_train, y_train))

# Writing model onto the file
model_dict = get_dict_from_model(
    model, x_index_to_word, x_word_to_index, y_index_to_word, y_word_to_index, learning_rate)
np.set_printoptions(threshold=np.nan, precision=15)
with open(model_file_name, "w+") as file:
    file.write(str(model_dict) + '\n')
