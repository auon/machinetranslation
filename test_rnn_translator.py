from RNNTranslator import RNNTranslator
from utils import get_model_from_dict, get_indices_from_words, get_words_from_indices
from numpy import *
import sys

hidden_layer_dim = 50
source_lang_filename = "manythings_500_unk_200.en"

if len(sys.argv) > 1:
    source_lang_filename = sys.argv[1] + ".en"

    if len(sys.argv) > 2:
        hidden_layer_dim = int(sys.argv[2])

model_file_name = "model_" + source_lang_filename[:-3] + "_hidden_" + str(hidden_layer_dim) + ".save"

print("Loading model from file: %s" % model_file_name)

with open(model_file_name, "r") as f:
    model_dict = eval(f.read().strip())

model = RNNTranslator(len(model_dict['x_index_to_word']),
                      len(model_dict['y_index_to_word']),
                      model_dict['y_word_to_index']['SENTENCE_END'])

get_model_from_dict(model, model_dict)

input_sent = input("Enter the sentence to translate: ")

sentence_start = "SENTENCE_START"
sentence_end = "SENTENCE_END"

input_sent = input_sent.strip().lower()
input_sent = "%s %s %s" % (sentence_start, input_sent, sentence_end)
tokenized_input_sent = input_sent.split(' ')
# print(tokenized_input_sent)
vectorized_input_sent = get_indices_from_words(model_dict['x_word_to_index'], tokenized_input_sent)
print(vectorized_input_sent)

predictions = model.predict(vectorized_input_sent)
print(predictions.shape)
print(predictions)
print(get_words_from_indices(model_dict['y_index_to_word'], predictions))
