import itertools
# import nltk
import numpy as np
import collections

sentence_start = "SENTENCE_START"
sentence_end = "SENTENCE_END"
unknown_word = "UNKNOWN_WORD"


# Generate features for the given file
def generate_features(file_name):
    with open(file_name, "r") as f:
        sentences = f.readlines()

        # if read_first_n_lines is not None:
        #     sentences = sentences[:read_first_n_lines]

        sentences = [x.strip().lower() for x in sentences]

        sentences = ["%s %s %s" % (sentence_start, x, sentence_end) for x in sentences]
        # print("Parsed %d sentences." % (len(sentences)))

    # tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in sentences]
    tokenized_sentences = [sentence.split(' ') for sentence in sentences]

    # word_freq = nltk.FreqDist(itertools.chain(*tokenized_sentences))
    # print("Found %d unique words tokens." % len(word_freq.items()))

    # index_to_word = word_freq.keys()
    index_to_word = list(set(itertools.chain(*tokenized_sentences)))

    vocab_size = len(index_to_word)

    word_to_index = dict([(w, i) for i, w in enumerate(index_to_word)])

    print("Vocab size for %s: %s" % (file_name, vocab_size))

    # print("\nExample sentence: '%s'" % sentences[1])
    # print("\nExample sentence after Pre-processing: '%s'" % tokenized_sentences[1])

    vectorized_sentences = np.asarray([[word_to_index[w] for w in sent] for sent in tokenized_sentences])

    # print("\nExample sentence after vectorization: '%s'" % vectorized_sentences[1])

    return vectorized_sentences, index_to_word, word_to_index, vocab_size


# Get features for the given file with the given word_to_index
def get_features(file_name, word_to_index):
    with open(file_name, "r") as f:
        sentences = f.readlines()

        sentences = [x.strip().lower() for x in sentences]

        sentences = ["%s %s %s" % (sentence_start, x, sentence_end) for x in sentences]

    tokenized_sentences = [sentence.split(' ') for sentence in sentences]

    unknown_word_index = 0
    if unknown_word in word_to_index:
        unknown_word_index = word_to_index[unknown_word]

    vectorized_sentences = np.asarray([[word_to_index.get(w, unknown_word_index) for w in sent] for sent in tokenized_sentences])

    return vectorized_sentences


# x_train, x_index_to_word, x_word_to_index, x_dimen = generate_features("dev.en")
#
# y_train, y_index_to_word, y_word_to_index, y_dimen = generate_features("dev.hi")

# f1 = open("dev.hi", "r")
# vocab = set()
# for row in f1:
#     x = row.strip().split(' ')
#     temp = set(list(vocab) + list(x))
#     vocab = temp
# print("No. of words in Hindi vocabulary: ", len(vocab))
# f2 = open("vocab.hi", "w")
# f2.write(str(vocab))
# f1.close()
# f2.close()


# Generate features for the given file
def generate_features_with_unknown_words(file_name, vocab_size):
    with open(file_name, "r") as f:
        sentences = f.readlines()

        sentences = [x.strip().lower() for x in sentences]

        sentences = ["%s %s %s" % (sentence_start, x, sentence_end) for x in sentences]

    tokenized_sentences = [sentence.split(' ') for sentence in sentences]

    word_list = list(itertools.chain(*tokenized_sentences))

    counter = collections.Counter(word_list)

    index_to_word = [t[0] for t in counter.most_common(vocab_size)]
    index_to_word.append(unknown_word)

    vocab_size = len(index_to_word)

    word_to_index = dict([(w, i) for i, w in enumerate(index_to_word)])

    print("Vocab size for %s: %s" % (file_name, vocab_size))

    # print("\nExample sentence: '%s'" % sentences[1])
    # print("\nExample sentence after Pre-processing: '%s'" % tokenized_sentences[1])

    vectorized_sentences = np.asarray([[word_to_index.get(w, word_to_index[unknown_word]) for w in sent] for sent in tokenized_sentences])

    # print("\nExample sentence after vectorization: '%s'" % vectorized_sentences[1])

    return vectorized_sentences, index_to_word, word_to_index, vocab_size
